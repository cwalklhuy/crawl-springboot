package com.crawler.crawlerspringboot.controller;

import com.crawler.crawlerspringboot.constant.URLConstant;
import com.crawler.crawlerspringboot.crawler.GroupNoteCrawler;
import com.crawler.crawlerspringboot.entity.GroupNote;
import com.crawler.crawlerspringboot.entity.Note;
import com.crawler.crawlerspringboot.entity.PerfumeFragrantica;
import com.crawler.crawlerspringboot.jsoup.JsoupConnection;
import com.crawler.crawlerspringboot.repository.GroupNoteRepository;
import com.crawler.crawlerspringboot.repository.NoteRepository;
import com.crawler.crawlerspringboot.repository.PerfumeFragranticaPagingRepository;
import com.crawler.crawlerspringboot.repository.PerfumeFragranticaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@RestController
public class Controller {

    private GroupNoteCrawler groupNoteCrawler;
    private GroupNoteRepository groupNoteRepository;
    private NoteRepository noteRepository;
    private final PerfumeFragranticaRepository perfumeFragranticaRepository;
    private final PerfumeFragranticaPagingRepository perfumeFragranticaPagingRepository;
    public Controller(GroupNoteCrawler groupNoteCrawler, GroupNoteRepository groupNoteRepository,
                      NoteRepository noteRepository, PerfumeFragranticaRepository perfumeFragranticaRepository,
                      PerfumeFragranticaPagingRepository perfumeFragranticaPagingRepository) {
        this.groupNoteCrawler = groupNoteCrawler;
        this.groupNoteRepository = groupNoteRepository;
        this.noteRepository = noteRepository;
        this.perfumeFragranticaRepository = perfumeFragranticaRepository;
        this.perfumeFragranticaPagingRepository = perfumeFragranticaPagingRepository;
    }

    @GetMapping("/crawl-note-by-note/{noteId}")
    public ResponseEntity<?> crawlOneNote(@PathVariable(value = "noteId") Long noteId) throws IOException, InterruptedException {
        Note note = noteRepository.getOne(noteId);
        if (note.getIsCrawled()) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
        }
        groupNoteCrawler.crawlAllPerfumeInOneNote(note);
        return ResponseEntity.ok("Crawled");
    }

    @GetMapping("/crawl-perfume-detail/{page}/{size}")
    public ResponseEntity<?> crawlPerfumeDetail(
            @PathVariable(value = "page") Integer page,
            @PathVariable(value = "size") Integer size
    ) throws IOException, InterruptedException {
        //0,10
        //11,100
        Sort sort = Sort.by(Sort.Direction.ASC, "perfumeFragranticaId");
        Pageable firstPageWithTenElements = PageRequest.of(page, size, sort);

        final Page<PerfumeFragrantica> perfumeFragranticas = perfumeFragranticaPagingRepository.findAll(firstPageWithTenElements);
        final List<PerfumeFragrantica> fragranticas = perfumeFragranticas.getContent();
        System.out.println(perfumeFragranticas.getTotalPages());
        for (PerfumeFragrantica fragrantica : fragranticas) {
            groupNoteCrawler.getPerfumeDetail(fragrantica.getDetailLink(), fragrantica);
            System.out.println("Crawling: " + fragrantica.getDetailLink() + " | ID: " + fragrantica.getPerfumeFragranticaId());
        }


        return ResponseEntity.ok().body("Show");
    }

    @GetMapping("/crawl-all-link-note")
    public String crawling() {
        JsoupConnection jsoupConnection = new JsoupConnection(URLConstant.URL);
        try {
            List<GroupNote> groupNotes = groupNoteCrawler.getListNote(jsoupConnection.getConnection());
            writeFragranceDetailToFile(groupNotes, "src/main/resources/crawlLinks.txt");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "crawl note success";
    }

    private void writeFragranceDetailToFile(List<GroupNote> groupNotes, String filename) throws
            IOException {
        FileWriter fileWriter = new FileWriter(filename);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int noOfNote = 0;
        try {
            for (GroupNote groupNote : groupNotes) {
                groupNoteRepository.save(groupNote);
                printWriter.println("Group note: " + groupNote.getName());
                printWriter.println("---------------------");
                for (Note note : groupNote.getNotes()) {
                    printWriter.println("Note name: " + note.getName());
                    printWriter.println("Detail link: " + note.getDetailLinkOfNote());
//                    printWriter.println("Icon link: " + note.get());
                    noOfNote += groupNote.getNotes().size();
                    printWriter.println("----");
                }
                if (!ObjectUtils.isEmpty(groupNote)) {
                    if (!CollectionUtils.isEmpty(groupNote.getNotes())) {
                        noteRepository.saveAll(groupNote.getNotes());
                    }
                }

                printWriter.println("Note count: " + groupNote.getNotes().size());
                printWriter.println("-------------End of one group note-------------");
                printWriter.println("-----------------------------------------------");
            }
            printWriter.println("Group note count: " + groupNotes.size());
            printWriter.println("Total note count: " + noOfNote);
            System.out.println("Done crawl notes list");

        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
            if (fileWriter != null) {
                fileWriter.close();
            }
        }

    }


}
