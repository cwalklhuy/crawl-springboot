package com.crawler.crawlerspringboot.crawler;

import com.crawler.crawlerspringboot.constant.GenderEnum;
import com.crawler.crawlerspringboot.constant.URLConstant;
import com.crawler.crawlerspringboot.entity.*;
import com.crawler.crawlerspringboot.jsoup.JsoupConnection;
import com.crawler.crawlerspringboot.repository.GroupNoteRepository;
import com.crawler.crawlerspringboot.repository.NotePerfumeRepository;
import com.crawler.crawlerspringboot.repository.NoteRepository;
import com.crawler.crawlerspringboot.repository.PerfumeFragranticaRepository;
import com.crawler.crawlerspringboot.util.TokenizerUtil;
import lombok.Getter;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Getter
//@Setter
@Service
public class GroupNoteCrawler {

    private GroupNoteRepository groupNoteRepository;
    private NoteRepository noteRepository;
    private PerfumeFragranticaRepository perfumeFragranticaRepository;
    private NotePerfumeRepository notePerfumeRepository;

    public GroupNoteCrawler(GroupNoteRepository groupNoteRepository, NoteRepository noteRepository, PerfumeFragranticaRepository perfumeFragranticaRepository, NotePerfumeRepository notePerfumeRepository) {
        this.groupNoteRepository = groupNoteRepository;
        this.noteRepository = noteRepository;
        this.perfumeFragranticaRepository = perfumeFragranticaRepository;
        this.notePerfumeRepository = notePerfumeRepository;
    }


    /**
     * Hàm trả về 1 list các GroupNote và từng note có trong mỗi GroupNote
     * Note: là những mùi hương cụ thể
     *
     * @param connection
     * @return
     * @throws IOException
     */
    public List<GroupNote> getListNote(Connection connection) throws IOException, InterruptedException {
        List<GroupNote> result = new ArrayList<>();
        String cssQuery = "a[href*=groupnotes_group]";

        //Bước 1: lấy hết cái đống html từ trang cần crawl ơ dạng doc
        Document document = connection.get();

        //Bước 2:Chọn ra element chứa dữ liệu mình cần
        Elements elements = document.select(cssQuery);
        Iterator<Element> iterator = elements.iterator();

        //1 Map chứa có key là tên của GroupNote và value là query của nó
        Map<String, String> listNoteName = new HashMap<>();

        while (iterator.hasNext()) {
            Element element = iterator.next();
            String query = getGroupNoteQuery(element.text());
            listNoteName.put(element.text(), query);//Đưa vào tên GroupNote và query của nó
        }//Tới đây là lấy đc 1 map chứa đủ hàng. Yeah


        /**
         * In ra ở console để kiểm tra =)
         */
        for (Map.Entry entry : listNoteName.entrySet()) {
            System.out.println("List này chứa= " + entry.getKey() + " and " + entry.getValue());
        }

        //Sau khi có đc các tên của hương tổng rồi bắt đầu lấy hết các note mà mỗi hương tổng có đc
        int count = 0;
        for (Map.Entry entry : listNoteName.entrySet()) {
            count++;

            //TODO: this is for test
//            if (count == 2) {
//                break;
//            }
            System.out.println("Mùi tổng là= " + entry.getKey());

            GroupNote groupNote = GroupNote.builder()
                    .name(entry.getKey().toString())
                    .build();
            //1 set chứa các note cụ thể có trong 1 mùi hương (GroupNote)
            Set<Note> noteList = new HashSet<>();

            // elements1 đang chứa các div là danh sách các note có trong fragance này
            Elements elements1 = document.select(entry.getValue().toString());

            Iterator<Element> elementIterator = elements1.iterator();
            while (elementIterator.hasNext()) {
                Element element = elementIterator.next();

                /**
                 * Chời 3s trước khi lấy danh sách các note của GroupNote đang cào
                 */

//                TimeUnit.SECONDS.sleep(3);
                Note note = getNote(element);
                note.setIsCrawled(false);
                note.setGroupNote(groupNote);
                noteList.add(note);
            }
            /**
             * Có đc mùi hương tổng và các note có trong nó
             * Ví dụ: mùi hương thu và số note có trong đó
             */

            groupNote.setNotes(noteList);
            //result là 1 list chứa các GroupNote
            result.add(groupNote);
            System.out.println("Group note number: " + count);
        }
        return result;
    }


    /**
     * Hàm lấy note ở đc liệt kê ở dưới hương tổng
     *
     * @param element
     * @return
     * @throws IOException
     */
    public Note getNote(Element element) throws IOException {
        String noteName = element.getElementsByTag("a").text();
        System.out.println("Mùi note là= " + noteName);
        String detailLinkOfNote = element.getElementsByTag("a").attr("href");
        System.out.println("detail link= " + detailLinkOfNote);
        String icon = element.getElementsByTag("img").attr("src");

        return Note.builder()
                .name(noteName)
                .detailLinkOfNote(detailLinkOfNote)
                .build();

    }

    /**
     * Tra về chuỗi query tương ứng với từng GroupNote
     *
     * @param GroupNote
     * @return
     */
    public String getGroupNoteQuery(String GroupNote) {
        String xPath = "";
        switch (GroupNote) {
            case "CITRUS SMELLS":
                ////*[@id="main-content"]/div[1]/div[1]/div/div[6]/div/div[1]
                //document.select("#docs > div:eq(1) > h4 > a").attr("href");
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(6) > div > div";
                break;
            case "FRUITS, VEGETABLES AND NUTS":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(9) > div > div";
                break;
            case "FLOWERS":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(12) > div > div";
                break;
            case "WHITE FLOWERS":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(15) > div > div";
                break;
            case "GREENS, HERBS AND FOUGERES":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(18) > div > div";
                break;
            case "SPICES":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(21) > div > div";
                break;
            case "SWEETS AND GOURMAND SMELLS":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(24) > div > div";
                break;
            case "WOODS AND MOSSES":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(27) > div > div";
                break;
            case "RESINS AND BALSAMS":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(30) > div > div";
                break;
            case "MUSK, AMBER, ANIMALIC SMELLS":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(33) > div > div";
                break;
            case "BEVERAGES":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(36) > div > div";
                break;
            case "NATURAL AND SYNTHETIC, POPULAR AND WEIRD":
                xPath = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(39) > div > div";
                break;
        }
        return xPath;
    }


    /**
     * Class này đã lấy đc 1 list nước hoa =)
     * Cần có thêm 1 hàm lấy chi tiết nước hoa từ cái link mà Perfume này có đc
     * để hoàn thiện dữ liệu
     * Vì trang web có khả năng chống crawl nên chỉ giới hạn 10 nước hoa trong
     *
     * @param connection
     * @return
     * @throws IOException
     */
    public Map<String, Object> getPerfumes(Connection connection, Note note) throws IOException, InterruptedException {
        Document document = connection.get();
        String cssQuery = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(4) > div > div.cell.medium-8.large-9.medium-order-2 > div > div";
        String aboveTen = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(4) > div > div.cell.medium-8.large-9.medium-order-2 > div > div:nth-child(2) > div > div:nth-child(1) > div:nth-child(4) > span";

        Elements elements = document.select(cssQuery);
        if (elements.size() <= 0) {
            cssQuery = "#main-content > div.grid-x.grid-margin-x > div.small-12.medium-8.large-9.cell > div > div:nth-child(3) > div > div.cell.medium-8.large-9.medium-order-2 > div > div";
            elements = document.select(cssQuery);
        }
        Iterator<Element> elementIterator = elements.iterator();
        Set<PerfumeFragrantica> perfumes = new HashSet<>();

        int count = 0;
        int finalNumber = 0;
        while (elementIterator.hasNext()) {

            TimeUnit.SECONDS.sleep(3);
            //Bây h ta chỉ lấy những nước hoa có số lượt commend trên 10

            Element e = elementIterator.next();
            Elements outside = e.getElementsByClass("flex-child-auto");
            Elements outsideCommendNumber = e.getElementsByTag("span");

            //lấy ảnh
            Elements img = e.getElementsByTag("img");
            String linkImg = "";//đường link đến ảnh nước hoa
            if (!img.isEmpty()) {
                Element elementImg = img.first();
                linkImg = elementImg.attr("src");
                System.out.println("ảnh= " + linkImg);
            }

            //lấy gender
            String genderText = "";
            Elements gender = e.getElementsByClass("flex-container align-justify ");
            for (int i = 0; i < gender.size(); i++) {
                Element element = gender.get(i);
                Elements span = element.getElementsByTag("span");
                if (span.first() != null) {
                    genderText = span.first().text();
                    System.out.println("gender= " + span.first().text());
                }
            }

            if (outside.first() != null) {
                String perfumeName = outside.first().text();//lấy tên nước hoa
                Elements a = outside.first().getElementsByTag("a");
                String href = a.attr("href");//link đi vào chi tiết của perfume
                System.out.println("perfume name= " + perfumeName);
                for (Element element : outsideCommendNumber) {
                    String styleValue = element.attr("style");
//                    if (styleValue.equals("font-size: small")) {
//                        if (Integer.valueOf(element.text()) >= 10) {
                    if (true) {
                        //Lấy thông tin tổng quát
                        PerfumeFragrantica perfume = PerfumeFragrantica.builder()
                                .name(perfumeName)
                                .image(linkImg)
                                .detailLink(URLConstant.origin + href)
                                .gender(GenderEnum.genderEnumFromString(genderText).getGenderNumber())
                                .build();

                        System.out.println("Số commned= " + element.text());
//                            TimeUnit.SECONDS.sleep(10);
//                            //Lấy thông tin chi tiết
//                            getPerfumeDetail(URLConstant.origin + href, perfume);
                        finalNumber++;

                        Set<NotePerfume> notePerfumes = new HashSet<>();
                        NotePerfume notePerfume = new NotePerfume();


//                            noteRepository.save(note);
                        perfume.setIsCrawled(true);
                        perfumes.add(perfume);//đưa vào set
                        Optional<PerfumeFragrantica> optionalPerfumeFragrantica = perfumeFragranticaRepository.findByDetailLink(URLConstant.origin + href);
                        if (optionalPerfumeFragrantica.isPresent()) {
                            perfume.setPerfumeFragranticaId(optionalPerfumeFragrantica.get().getPerfumeFragranticaId());
                        }
                        final PerfumeFragrantica perfumeFragrantica = perfumeFragranticaRepository.save(perfume);
                        final Optional<NotePerfume> notePerfume1 = notePerfumeRepository.findByPerfumeFragranticaAndNote(perfumeFragrantica, note);
                        if (!notePerfume1.isPresent()) {
                            NotePerfumePK notePerfumePK = NotePerfumePK.builder()
                                    .perfumeFragranticaId(perfumeFragrantica.getPerfumeFragranticaId())
                                    .noteId(note.getNoteId())
                                    .build();
                            //update
                            final NotePerfume notePerfume2 = NotePerfume.builder()
                                    .notePerfumePK(notePerfumePK)
                                    .note(note)
                                    .perfumeFragrantica(perfumeFragrantica)
                                    .build();
                            notePerfumeRepository.save(notePerfume2);
                        }
//                            notePerfumes.add(notePerfume);
                        note.setIsCrawled(true);

                    }
//                    } else {
//                        System.out.println("số commend nhỏ ");
//                    }


                }
                count++;
                System.out.println("sô lượng: " + count);
            }
            //Kết thúc phần test
        }

        System.out.println("Tổng sô có thể crawl= " + finalNumber);
        Map<String, Object> result = new HashMap<>();
        result.put("perfume_set", perfumes);
        result.put("updated_note", note);
        return result;
    }

    /**
     * Hàm lấy detail của chai nước hoa
     * Nếu như có rating và voter thì mới lưu vào DB
     */
    public void getPerfumeDetail(String connection, PerfumeFragrantica perfume) throws IOException, InterruptedException {
        System.out.println("Đây là nước hoa: " + perfume.getName());
        System.out.println("link ảnh= " + perfume.getImage());

        JsoupConnection jsoupConnection = new JsoupConnection(connection);
        Connection con = jsoupConnection.getConnection();//bước 1 mở cổng
        Document document = con.get();

        //Liệt kê lần lượt tên từng lvl note
//        String topNote = "";
//        String middleNote = "";
//        String baseNote = "";
        Map<String, String> notesString = new HashMap<>();
//        Elements damnit=document.select("#note75 > img");
//        System.out.println("hàng to= "+damnit.attr("bt-xtitle"));
        //Phần bên ngoài của note trong nước hoa
        String outsidePerfumePyramid = "#col1 > div > div > div:nth-child(13) > div:nth-child(1)";
        Element div = document.select(outsidePerfumePyramid).first();
//#col1 > div > div > div:nth-child(12) > div:nth-child(1)
        if (div == null) {
            outsidePerfumePyramid = "#col1 > div > div > div:nth-child(12) > div:nth-child(1)";
            div = document.select(outsidePerfumePyramid).first();
        }
//        else {
        final Element h3Element = div.select("> h3").first();

        System.out.println(h3Element.text());
        // Get all noteName from Link perfume
        if (h3Element.text().equals("Perfume Pyramid") || h3Element.text().equals("Fragrance Notes")) {
            final Elements pElements = div.select("> p");
            final Iterator<Element> iterator = pElements.iterator();
            while (iterator.hasNext()) {
                final Element element = iterator.next();
                final Elements elements = element.select("> span");
                final Iterator<Element> iterator1 = elements.iterator();
                while (iterator1.hasNext()) {
                    final Element element1 = iterator1.next();
//                    final String originalNoteId = element1.getElementsByTag("")
                    final String noteName = element1.getElementsByTag("img").attr("title");
                    final String originalNoteId = element1.attr("title");
                    notesString.put(noteName, originalNoteId);
                    System.out.println(noteName);
                }
            }
        }

        notesString.forEach((k,v) -> System.out.println("Note: " + k + " original id: " + v));


        String cssQueryRating = "#col1 > div > div > div:nth-child(12) > p > span:nth-child(1)";
        String cssQueryVoter = "#col1 > div > div > div:nth-child(12) > p > span:nth-child(3)";

        Element elements1 = document.getElementById("userMainNotes");

        if (!elements1.getElementsByTag("div").isEmpty()) {
            Elements element = elements1.getElementsByTag("img");
            System.out.println("số lượng note = " + notesString.size());
        } else {
            System.out.println("Damnit");
        }


        /**
         * Lấy rating và voter
         */
        Elements elementsR = document.select(cssQueryRating);
        Elements elementsV = document.select(cssQueryVoter);

        /**
         * Nếu như 2 cái trường này ko có giá trị thì ko cần phải crawl cái nước hoa này
         */
        if (!elementsR.isEmpty() && !elementsV.isEmpty()) {
            String rating = elementsR.first().text();
            System.out.println("rating= " + rating);
            String voter = elementsV.first().text();
            System.out.println("voter= " + voter);
            perfume.setVote(Double.parseDouble(rating));
            perfume.setVoters(Integer.parseInt(voter));
        }
        /**
         * Kết thúc lấy rating và voter
         */

        /**
         * Lấy độ lưu hương
         */
        String poor = document.select("#col1 > div > div > div.longSilBox.effect6 > div:nth-child(3) > div > table > tbody > tr:nth-child(1) > td.ndSum").first().text();
        String weak = document.select("#col1 > div > div > div.longSilBox.effect6 > div:nth-child(3) > div > table > tbody > tr:nth-child(2) > td.ndSum").first().text();
        String moderate = document.select("#col1 > div > div > div.longSilBox.effect6 > div:nth-child(3) > div > table > tbody > tr:nth-child(3) > td.ndSum").first().text();
        String longLasting = document.select("#col1 > div > div > div.longSilBox.effect6 > div:nth-child(3) > div > table > tbody > tr:nth-child(4) > td.ndSum").first().text();
        String veryLongLasting = document.select("#col1 > div > div > div.longSilBox.effect6 > div:nth-child(3) > div > table > tbody > tr:nth-child(5) > td.ndSum").first().text();

        perfume.setLongevityPoor(Integer.valueOf(poor));
        perfume.setLongevityModerate(Integer.valueOf(moderate));
        perfume.setLongevityLongLasting(Integer.valueOf(longLasting));
        perfume.setLongevityVeryLongLasting(Integer.valueOf(veryLongLasting));
        perfume.setLongevityWeak(Integer.valueOf(weak));
//            System.out.println("poor= " + perfume.setLongevityPoor() + "\n" + " moderate= " + perfume.setLongevityModerate() + "\n" + " longlast= " + perfume.getLongevity_longlasting() + "\n" + " very= " + perfume.getLongevity_verylonglasting() + "\n" + " weak= " + perfume.getLongevity_weak());

        /**
         * Lấy độ tỏa hương
         */
        String soft = document.select("#col1 > div > div > div.longSilBox.effect6 > div.divSil > table > tbody > tr > td > table > tbody > tr:nth-child(1) > td.ndSum").first().text();
        String moderateV = document.select("#col1 > div > div > div.longSilBox.effect6 > div.divSil > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td.ndSum").first().text();
        String heavy = document.select("#col1 > div > div > div.longSilBox.effect6 > div.divSil > table > tbody > tr > td > table > tbody > tr:nth-child(3) > td.ndSum").first().text();
        String enormous = document.select("#col1 > div > div > div.longSilBox.effect6 > div.divSil > table > tbody > tr > td > table > tbody > tr:nth-child(4) > td.ndSum").first().text();

        perfume.setSillageSoft(Integer.valueOf(soft));
        perfume.setSillageEnorMous(Integer.valueOf(enormous));
        perfume.setSillageModerate(Integer.valueOf(moderateV));
        perfume.setSillageHeavy(Integer.valueOf(heavy));
        //save to DB

        final PerfumeFragrantica perfumeFragrantica = perfumeFragranticaRepository.save(perfume);
        List<Note> notes = new ArrayList<>();
        notesString.forEach((k,v) -> {
            final List<Note> notes1 = noteRepository.findByName(k);
//            final Optional<Note> noteFoundByName = noteRepository.findByName(k);
            final Optional<Note> note = noteRepository.findByOriginalNoteId(v);
            if (note.isPresent()) {
                notes.add(note.get());
                if (!notes1.isEmpty()) {
//                    n.setOriginalNoteId(originalNoteId);
                    if (!notes1.get(0).equals(note.get())) {
                        System.out.println("=========WARNING: Note Original Id vs Note in db is not the same ========");
                    }
                }
//                notes.add(n);
            }
        });

        notes.forEach(note -> {
            final Optional<NotePerfume> notePerfume = notePerfumeRepository.findByPerfumeFragranticaAndNote(perfumeFragrantica, note);
            if (notePerfume.isPresent()) {
                //Update
            } else {
                NotePerfumePK notePerfumePK = NotePerfumePK.builder()
                        .perfumeFragranticaId(perfumeFragrantica.getPerfumeFragranticaId())
                        .noteId(note.getNoteId())
                        .build();
                final NotePerfume notePerfume2 = NotePerfume.builder()
                        .notePerfumePK(notePerfumePK)
                        .note(note)
                        .perfumeFragrantica(perfumeFragrantica)
                        .build();
                notePerfumeRepository.save(notePerfume2);
            }
        });
//        }
    }

    /**
     * Chỉ mang mục đích để testing
     *
     * @throws IOException //
     */
//    public static void main(String[] args) throws IOException, InterruptedException {
////        JsoupConnection jsoupConnection = new JsoupConnection("https://www.fragrantica.com/notes/Bitter-Orange-79.html");
////
////        Connection con = jsoupConnection.getConnection();
//
//
//        /**
//         * Test để lấy cái score của từng note trong nước hoa
//         */
//        GroupNoteCrawler craw = new GroupNoteCrawler();
//
////        Note note = craw.getNoteFunction().findById(7).get(0);
////        craw.crawlAllPerfumeInOneNote(note);
//        Note note2 = craw.getNoteFunction().findById(11).get(0);
//        craw.crawlAllPerfumeInOneNote(note2);
//
//        TimeUnit.SECONDS.sleep(300);
//        Note note3 = craw.getNoteFunction().findById(12).get(0);
//        craw.crawlAllPerfumeInOneNote(note3);
//
//        TimeUnit.SECONDS.sleep(300);
//        Note note4 = craw.getNoteFunction().findById(13).get(0);
//        craw.crawlAllPerfumeInOneNote(note4);
//    }
    public void crawlAllPerfumeInOneNote(Note e) throws IOException, InterruptedException {

        if (e.getIsCrawled()) {
            System.out.println("Note is crawled");
            return;
        }
//        List<NotePerfume> notePerfumes = (List<NotePerfume>) notePerfumeRepository.findAllByNote_NoteId(e.getNoteId());
//        if (notePerfumes.size() > 0) {
//            List<Integer> perfumeIds = new ArrayList<>();
//            for (NotePerfume np : notePerfumes) {
//                perfumeIds.add(np.getPerfumeFragrantica().getPerfumeFragranticaId());
//            }
//            notePerfumeRepository.deleteAllByNote_NoteId(e.getNoteId());
//            perfumeFragranticaRepository.deleteAllByPerfumeFragranticaIdIn(perfumeIds);
//        }

        Set<Note> updatedNoteSet = new HashSet<>();
        String detailLinkOfNote = e.getDetailLinkOfNote();
        Connection detail = new JsoupConnection(detailLinkOfNote).getConnection();

        TimeUnit.SECONDS.sleep(5);

        Map<String, Object> perfumeAndNotePerfumeResult = getPerfumes(detail, e);


        if (updatedNoteSet.contains(e)) updatedNoteSet.remove(e);
        updatedNoteSet.add(e);

//        }//end for note : GroupNote.getNoteSet
//        }//end for GroupNote : GroupNote

    }

    /**
     * Thời gian mở connection vào trong từng note: 2s
     * Thời gian chờ để lấy thum: 5s
     * Thời gian chờ để danh sách nước hoa: 10s
     * Thời gian để mở connect để lấy detail nước hoa: 10s cho số chẵn và 2s cho số lẻ
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void startCrawling() throws IOException, InterruptedException {
        JsoupConnection jsoupConnection = new JsoupConnection(URLConstant.URL);

        List<GroupNote> GroupNotes = getListNote(jsoupConnection.getConnection());

        List<Set<PerfumeFragrantica>> listOfPerfumeSets = new ArrayList<>();

        int count = 0;
        List<GroupNote> updatedGroupNotes = new ArrayList<>();

        for (GroupNote GroupNote : GroupNotes) {
            Set<Note> updatedNoteSet = new HashSet<>();
//            GroupNote GroupNoteInDB = GroupNoteFunction.findByName(GroupNote.getName()).get(0);
//            if (GroupNoteInDB == null) {
            groupNoteRepository.save(GroupNote);
            Set<Note> noteSetInGroupNote = (Set<Note>) GroupNote.getNotes();
            for (Note e : noteSetInGroupNote) {
//                List<Note> noteListInDB = noteFunction.findByNameAndIcon(e.getName(), e.getIcon());
//                if (noteListInDB.size() <= 0) {
                count++;
                /**
                 * Link chi tiêt để đi vào từng note, vào trong đó sẽ cho ta thấy
                 * có bao nhiêu nước hoa được chứa trong note đó
                 * chờ khoảng 2s
                 */
//                TimeUnit.SECONDS.sleep(2);
                String detailLinkOfNote = e.getDetailLinkOfNote();
                Connection detail = new JsoupConnection(detailLinkOfNote).getConnection();

                /**
                 * Trước khi lấy thumb hãy chờ 5s
                 */

                TimeUnit.SECONDS.sleep(5);
                //Ở bước dưới ta đã cho vào thumbnail của note đang cào
//                    for (Set<Thumbnail> thumbnailSet : listOfThumbnailSets) {
//                        for (Thumbnail thumbnail : thumbnailSet) {
//                            saveThumbnailToDB(thumbnail);
//                        }
//                    }
                /**
                 * trước khi vào cào nước hoa thì hãy đợi 10s
                 */
                TimeUnit.SECONDS.sleep(10);

                //Đã có được 1 danh sách chứa các nước hoa từ link trên và chi tiêt của nó =)
                Map<String, Object> perfumeAndNotePErfumeResult = getPerfumes(detail, e);
//                    Set<Perfume> perfumes = (Set<Perfume>) perfumeAndNotePErfumeResult.get("perfume_set");
//                    listOfPerfumeSets.add(perfumes);
//                    e = (Note) perfumeAndNotePErfumeResult.get("updated_note");


                if (updatedNoteSet.contains(e)) updatedNoteSet.remove(e);
                updatedNoteSet.add(e);

            }//end for note : GroupNote.getNoteSet
        }//end for GroupNote : GroupNote

    }
}
