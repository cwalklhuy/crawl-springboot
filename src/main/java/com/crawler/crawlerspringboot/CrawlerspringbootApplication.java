package com.crawler.crawlerspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrawlerspringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrawlerspringbootApplication.class, args);
    }

}
