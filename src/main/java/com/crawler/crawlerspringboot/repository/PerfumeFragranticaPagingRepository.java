package com.crawler.crawlerspringboot.repository;

import com.crawler.crawlerspringboot.entity.PerfumeFragrantica;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerfumeFragranticaPagingRepository extends PagingAndSortingRepository<PerfumeFragrantica, Long> {

    List<PerfumeFragrantica> findAllByPerfumeFragranticaIdBetween(Long from, Long to);
}
