package com.crawler.crawlerspringboot.repository;

import com.crawler.crawlerspringboot.entity.GroupNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupNoteRepository extends JpaRepository<GroupNote, Long> {
}
