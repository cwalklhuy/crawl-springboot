package com.crawler.crawlerspringboot.repository;

import com.crawler.crawlerspringboot.entity.PerfumeFragrantica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PerfumeFragranticaRepository extends JpaRepository<PerfumeFragrantica, Long>{
    Optional<PerfumeFragrantica> findByDetailLink(String detailLink);


}
