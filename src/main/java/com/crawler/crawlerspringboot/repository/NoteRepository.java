package com.crawler.crawlerspringboot.repository;

import com.crawler.crawlerspringboot.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

//    @Query("select p from Note p where p.name in :noteNames")
//    List<Note> findByNameIn(@Param("noteNames") List<String> noteNames);

    List<Note> findByName(String noteName);
    Optional<Note> findByOriginalNoteId(String originalNoteId);

}
