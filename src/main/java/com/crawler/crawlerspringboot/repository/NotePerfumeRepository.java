package com.crawler.crawlerspringboot.repository;

import com.crawler.crawlerspringboot.entity.Note;
import com.crawler.crawlerspringboot.entity.NotePerfume;
import com.crawler.crawlerspringboot.entity.NotePerfumePK;
import com.crawler.crawlerspringboot.entity.PerfumeFragrantica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotePerfumeRepository extends JpaRepository<NotePerfume, NotePerfumePK> {

    Optional<NotePerfume> findByPerfumeFragranticaAndNote(PerfumeFragrantica perfumeFragrantica, Note note);

}
