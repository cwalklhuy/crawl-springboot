package com.crawler.crawlerspringboot.constant;

import lombok.Getter;

@Getter
public enum GenderEnum {
    MALE(1, "male"),
    FEMALE(2, "female"),
    UNISEX(3, "unisex"),
    ;

    private Integer genderNumber;
    private String genderString;

    GenderEnum(Integer genderNumber, String genderString) {
        this.genderNumber = genderNumber;
        this.genderString = genderString;
    }

//    public static fromStringToNum(String genderString) {
//        for (GenderEnum)
//    }
    public static GenderEnum genderEnumFromString(String genderString) {
        switch (genderString) {
            case "female":
                return FEMALE;
            case "male":
                return MALE;
            case "unisex":
                return UNISEX;
        }
        return null;
    }
}
