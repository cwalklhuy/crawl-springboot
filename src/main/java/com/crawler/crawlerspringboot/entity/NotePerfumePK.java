package com.crawler.crawlerspringboot.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class NotePerfumePK implements Serializable {

    @Column
    private Long perfumeFragranticaId;

    @Column
    private Long noteId;

    @Override
    public int hashCode() {
        return Objects.hash(perfumeFragranticaId, noteId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        NotePerfumePK notePerfumePK = (NotePerfumePK) obj;
        return perfumeFragranticaId.equals(notePerfumePK.perfumeFragranticaId) &&
                noteId.equals(notePerfumePK.noteId);
    }
}
