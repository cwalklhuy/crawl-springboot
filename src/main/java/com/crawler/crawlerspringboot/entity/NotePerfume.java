package com.crawler.crawlerspringboot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "note_perfume")
public class NotePerfume {

    @EmbeddedId
    private NotePerfumePK notePerfumePK;

    @JsonIgnore
    @MapsId("noteId")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "note_id", referencedColumnName = "note_id")
    private Note note;

    @JsonIgnore
    @MapsId("perfumeFragranticaId")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "perfume_fragrantica_id", referencedColumnName = "perfume_fragrantica_id")
    private PerfumeFragrantica perfumeFragrantica;
}
