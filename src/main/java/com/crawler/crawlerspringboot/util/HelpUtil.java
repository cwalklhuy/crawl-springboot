package com.crawler.crawlerspringboot.util;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Iterator;

public class HelpUtil {
    /**
     * Hàm này sẽ trả về 1 con duyệt Elements
     * Đỡ phải viết đi viết lại nhiều lần
     * @param connection
     * @param query
     * @return
     * @throws IOException
     */
    public static Iterator<Element> provideIterator(Connection connection, String query) throws IOException {
        Document document = connection.get();

        String cssQuery = query;
        Elements thumpList = document.select(cssQuery);

        return thumpList.iterator();
    }

    /**
     * Trả về 1 Element dựa trên query
     * @param connection
     * @param query
     * @return
     */
    public static Element getElement(Connection connection,String query) throws IOException {
        Document document = connection.get();

        String cssQuery = query;
        return document.select(cssQuery).first();
    }
}
