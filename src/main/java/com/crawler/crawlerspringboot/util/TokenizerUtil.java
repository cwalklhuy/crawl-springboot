package com.crawler.crawlerspringboot.util;

import java.util.StringTokenizer;

public class TokenizerUtil {
    public static String getOriginalNoteIdFromUrl(String url) {
//        String url = "https://www.fragrantica.com/notes/Peach-Leaf-934.html";
        StringTokenizer tokenizer = new StringTokenizer(url, "/");
        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
//            System.out.println(token);
            if (token.contains(".html")) {
                int indexOfHtml = token.indexOf(".html");
                String originalNoteId = token.substring(0, indexOfHtml);
//                System.out.println(originalNoteId);
                //get number id
                StringTokenizer tokenizer1 = new StringTokenizer(originalNoteId, "-");
                while(tokenizer1.hasMoreElements()) {
                    String token1 = tokenizer1.nextToken();
//                    System.out.println(token1);
                    if (token1.matches("-?\\d+(\\.\\d+)?")){
                        System.out.println("Matched: " + token1);
                        return token1;
                    }
                }
//                return originalNoteId;
            }
        }
        return null;
    }
}
